//
//  AppDelegate.h
//  test
//
//  Created by xiangwenjun on 16/12/12.
//  Copyright © 2016年 xiangwenjun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

