//
//  main.m
//  test
//
//  Created by xiangwenjun on 16/12/12.
//  Copyright © 2016年 xiangwenjun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
